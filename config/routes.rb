Rails.application.routes.draw do

  #meme routes
  post "/update_meme/:id", to: "memes#update"
  get "/public_memes", to: "memes#show_public"
  get "/meme/:id", to: "memes#show"
  post "/create_meme", to: "memes#create"
  post "/send_email/:id", to: "memes#send_mail"
  #get "/all_memes", to: "memes#index"

 #user routes
  get '/users', to: 'users#index'
  post "/login", to: "users#login"
  post "/logout", to: "users#logout"
  post "/create_user", to: "users#create"
  delete "/delete_acount", to: "users#destroy"
  get "/my_memes", to: "users#my_memes"


  #vote routes
  post "/vote", to: "votes#create"
  get "/votes", to: "votes#index"

  #category routes
 	get "/animals", to: "categories#memes_animal"
 	get "/movies", to: "categories#memes_movie"
 	get "/people", to: "categories#memes_person"
	get "/others", to: "categories#memes_other"
end
