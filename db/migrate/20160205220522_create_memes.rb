class CreateMemes < ActiveRecord::Migration
  def change
    create_table :memes do |t|
      t.references :user, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true
      t.string :mail_to
      t.string :meme_name
      t.string :imagen_id
      t.string :top_text
      t.string :bottom_text
      t.integer :ranking
      t.string :save_in
      t.string :url_memecaptain
      t.string :private

      t.timestamps null: false
    end
  end
end
