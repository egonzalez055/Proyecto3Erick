class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :animals
      t.string :movies
      t.string :people
      t.string :other

      t.timestamps null: false
    end
  end
end
