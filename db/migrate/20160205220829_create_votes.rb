class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.references :meme, index: true, foreign_key: true
      t.integer :ranking

      t.timestamps null: false
    end
  end
end
