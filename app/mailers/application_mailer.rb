class ApplicationMailer < ActionMailer::Base
  default from: "memecaptain_API_erick"
  layout 'mailer'
end
