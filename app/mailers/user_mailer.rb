class UserMailer < ApplicationMailer

	def simple_email(mailed_to)
  	@mailed_to = mailed_to
  	mail(to: @mailed_to) do |format|
  	format.html {render 'user_mailer'}
  end
  end

  def attached_email(file_name, email) 	
 		mail(to: email, subject: "new mail" ) do |format|
 		format.html {render 'user_mailer'}
 		attachments["new_meme"] = File.read("img/" + file_name)
  end
	end


end
