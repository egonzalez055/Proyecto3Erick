class CategoriesController < ApplicationController

	rescue_from ActiveRecord::RecordNotFound, with: :r_not_found
	
	def r_not_found(error)
		render json: {error: error.message}, status: :r_not_found
	end

	def index
		votes = Category.all
		render json: votes
	end

	def show #id 1 is animal id 2 is movies id 3 is people id 4 is other
		b = Category.find(params[:id])
		render json: b
	end
		
		def memes_animal
			a = Meme.where(category_id: 1)
			render json: a
		end

	def memes_movie
		a = Meme.where(category_id: 2)
		render json: a
	end

	def memes_person
		a = Meme.where(category_id: 3)
		render json: a
	end

	def memes_other
		a = Meme.where(category_id: 4)
		render json: a
	end

		

end