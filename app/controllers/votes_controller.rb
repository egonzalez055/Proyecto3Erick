class VotesController < ApplicationController

	rescue_from ActiveRecord::RecordNotFound, with: :r_not_found
	
	def r_not_found(error)
		render json: {error: error.message}, status: :r_not_found
	end

	def index
		votes = Vote.all
		render json: votes
	end

	def show
		b = Vote.find(params[:id])
		render json: b
	end

	def create
		b = Vote.new(allowed_params)
		if b.save
			render json: {menssage: "you voted #{b.ranking} for #{b.meme_id}"}
		else render json: {menssage: "not create"}
		end
	end







	protected

	def allowed_params
		params.require(:vote).permit(:meme_id, :ranking)
	end
end