class MemesController < ApplicationController

	rescue_from ActiveRecord::RecordNotFound, with: :r_not_found
	  before_action :authenticate, only: :create

	def show_public
		feo = Meme.where(private: "flase") #deberia ser false
		render json: feo
	end
	
	def index
		m = Meme.all
		render json: m
		
	end

	def show 
		@meme = Meme.find(params[:id])
		render json: @meme
	end

	def create
		b = Meme.new(allowed_params)
		b.create_meme
		b.user_id = User.where(login_params).take.id

		#b.save_picture(b.top_text)
		#b.user = @user
		#b.user_id = @user[:id]

		if b.save
			render json: b
		else render json: {menssage: "pa q quiere saber eso jaja saludos"}
		end
		b.save_picture
	end

	def send_mail
		@meme = Meme.find(params[:id])
		@meme.update(email_params) #meme.top_text deberia ser el nombre del archivo y meme.bottom_text deberia ser meme.email
		UserMailer.attached_email(@meme.meme_name, @meme.mail_to).deliver_now
		render json: {menssage: "has been sent to #{@meme.meme_name}"}
	end


	def update 
		b = Meme.find(params[:id])
		#if b.save
		b.update(params_update)
			render json: b
		#else render json: {menssage: "pa q quiere saber eso jaja saludos"}
		#Send
	end

	#def destroy
	#	b = Meme.find(params[:id]).destroy
	#	if b.delete
	#		render json: {menssage: "has been destroyed"}
	#	else render json: {menssage: "still here"}
	#	end
	#end

	protected

	def r_not_found(error)
		render json: {error: error.message}, status: :r_not_found
	end

	def login_params
		params.require(:user).permit(:name)
	end

	def allowed_params
		params.require(:meme).permit(:imagen_id, :bottom_text, :top_text, :private, :meme_name)
	end

	def params_update
		params.require(:meme).permit(:mail_to, :meme_name)	
	end

	def email_params
		params.require(:meme).permit(:mail_to, :meme_name)
	end
end