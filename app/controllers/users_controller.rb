require 'securerandom'
class UsersController < ApplicationController
	 before_action :authenticate, except:[:create, :login]

	rescue_from ActiveRecord::RecordNotFound, with: :r_not_found

	def my_memes
		m = Meme.where(user_id: @user.id)
		render json: m
		
	end


	def login
		@user = User.where(login_params).take
		if @user
				@user.token = SecureRandom.uuid.gsub(/\-/,'')
				@user.save
				render json: @user
			else
				render json: {menssage: "user not found"}	
		end
		return @user
	end
  
	def logout
		@user.token = nil
		@user.save
		render json: @user
	end

	#def send_mail(file_name, route)
		#UserMailer.attached_email(file_name, route, @user.name).deliver_now	
	#end

	def index
	#if @user.admi?
	#@users = User.all
		
		render json: @user
	#else 
	#render json: @user
  #end
	end


	#def show
		#b = User.find(params[:name])
		#render json: b
	#end

	def create
		b = User.new(allowed_params) 
		if b.save
			render json: b
		else render json: {menssage: "pa q quiere saber eso jaja saludos"}
		end
	end


	def update 
		render nothing: true
		b = User.find(params[:id]).update(allowed_params)
		if b.save
			render json: {menssage: "has been updated"}
		else render json: {menssage: "pa q quiere saber eso jaja saludos"}
		end
	end

	def destroy
		@user.destroy
		if @user.delete
			render json: {menssage: "has been destroyed"}
		else render json: {menssage: "pa q quiere saber eso jaja saludos"}
		end
	end


	protected

	def login_params
		params.require(:user).permit(:name, :token)
	end

	def r_not_found(error)
		render json: {error: error.message}, status: :r_not_found
	end
	
	def allowed_params
		params.require(:user).permit(:name)
	end

end