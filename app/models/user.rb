class User < ActiveRecord::Base
	has_many :memes
	accepts_nested_attributes_for :memes
	validates :name, presence: true
end
