class Meme < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  has_many :votes

  before_save :calculate_ranking
  before_create :saved_in
  before_create :check_boolean

  def calculate_ranking
  	total = 0
  	self.votes.each do |s|
  	total += s.ranking
  	self.ranking = total
  end
  end

  def create_meme
  	json_meme = {
  "private": false,
  "src_image_id": self.imagen_id,
  "captions_attributes": [
    {
      "text": self.top_text,
      "top_left_x_pct": 0.05,
      "top_left_y_pct": 0.0,
      "width_pct": 0.9,
      "height_pct": 0.25
    },
    {
      "text": self.bottom_text,
      "top_left_x_pct": 0.05,
      "top_left_y_pct": 0.75,
      "width_pct": 0.9,
      "height_pct": 0.25
    }
  ]
}

    json_response= JSON.parse(RestClient.post("https://memecaptain.com/gend_images", json_meme.to_json, content_type: :json, accept: :json))
    self.url_memecaptain = json_response["status_url"] 	
  end

  def save_picture
  	 File.open("img/"+ self.meme_name, "wb"){|file| file.write(RestClient.get(self.url_memecaptain))}
  end

  def saved_in 
    self.save_in = "img/#{self.meme_name}"
  end

  def check_boolean
    if self.private == "true" || self.private == "false"
    else
      false
    end
  end
end
